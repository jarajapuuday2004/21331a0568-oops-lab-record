import java.lang.*;

class St {

    private String fullName;

    private double semPercentage;

    private String collegeName;

    private int collegeCode;


    // default constructor

    public St(){

        collegeName = "MVGR";

        collegeCode = 33;

        System.out.println("Default Constructor called !");

        System.out.println("College Name :"+collegeName);

        System.out.println("College Code :"+collegeCode);

    }


    // parameterized constructor

    public St(String fName, double percentage) {

        fullName=fName ;

        semPercentage=percentage;

        System.out.println("\nParameterized constructor called !");

        System.out.println("Full name :" + fullName);

        System.out.println("Sem Percentage :"+semPercentage);

    }


    // destructor

     protected void finalize() {

        System.out.println("destroyed !");

    }

   

    public static void main(String[] args) {

        St m = new St();

        St s = new St("Uday",86.8);

    }

}

