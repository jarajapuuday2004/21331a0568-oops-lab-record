// Parent class
class AquaticAnimal {
    private String name;
  
    public AquaticAnimal(String name) {
      this.name = name;
      System.out.println("AquaticAnimal Called");
    }
  
    public void eat() {
      System.out.println(name + " is eating.");
    }
  
    protected void swim() {
      System.out.println(name + " is swimming");
    }
  
    private void breathe() {
      System.out.println(name + " is breathing.");
    }
  }
  
  // Child class
  class Fish extends AquaticAnimal {
    public Fish(String name) {
      super(name);
    }
  }
  class Ppp{
    public static void main(String[] args) {
      Fish myFish = new Fish("Uday");
      myFish.eat();
      myFish.swim();
      //myFish.breathe();//can't Access 
  }
  }