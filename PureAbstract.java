interface Tiffin {
    void eat();
}


class Idli implements Tiffin {
    public void eat() {
        System.out.println("Eat Idli for tiffin ");
    }
}


class Dosa implements Tiffin {
    public void eat() {
        System.out.println("Eat Dosa for tiffin ");
    }
}


public class PureAbstract {
    public static void main(String[] args) {
        Tiffin obj1 = new Idli();
        Tiffin obj2 = new Dosa();
        
        obj1.eat(); 
        obj2.eat(); 
    }
}


