import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;;
class Login{
    Login(){
        Frame fr = new Frame();
        fr.setTitle("Login Form");
        fr.setSize(400, 400);
        fr.setVisible(true);
        fr.setBackground(Color.ORANGE);
        fr.setLayout(null);
        
        Font f = new Font("times new roman", Font.BOLD, 15);
        
        Label l1 = new Label("USER LOGIN");
        l1.setFont(f);
        l1.setBounds(130, 50, 100, 30);

        Label l2 = new Label("Username");
        l2.setBounds(70, 100, 70, 30);
        l2.setForeground(Color.BLACK);
        TextField t1 = new TextField();
        t1.setBounds(160, 105, 100, 30);

        Label l3 = new Label("Password");
        l3.setBounds(70, 150, 70, 30);
        TextField t2 = new TextField();
        t2.setBounds(160, 155, 100, 30);
        t2.setEchoChar('*');
        
        Button b = new Button("Login");
        b.setFont(f);
        b.setBounds(130, 205, 70, 30);

        Label l4 = new Label();
        l4.setFont(f);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String user = t1.getText();
                String pass = t2.getText();
                if (user.equals("21331A0583") || user.equals("21331A0584") && pass.equals("BUBUDUDU")){
                    l4.setText("Login SuccessFull");
                    l4.setBounds(110,250, 150, 30);
                    l4.setForeground(Color.green);
                }
                else{
                    l4.setText("Invalid Username or Password");
                    l4.setBounds(70,250, 250, 30);
                    l4.setForeground(Color.RED);
                }
            }
        });
        fr.add(l1);
        fr.add(l2);
        fr.add(l3);
        fr.add(t1);
        fr.add(t2);
        fr.add(b);
        fr.add(l4);
        fr.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                fr.dispose();
            }
        });

    }
    public static void main(String[] args) {
        Login log = new Login();
    }
}