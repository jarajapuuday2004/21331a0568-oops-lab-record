public class BoxingUnBoxing {
        public static void main(String[] args){
        int counter = 30;
        Integer boxedCounter = counter;
        System.out.println("Boxed Counter: " + boxedCounter);
        //UnBoxing
        int unboxedCounter = boxedCounter;
        System.out.println("Unboxed Counter: " + unboxedCounter);
        }
    }