#include <iostream>
using namespace std;
class Job{
    public:
    virtual void work()=0;

    void salary(){
        cout << "Earning enough money..." << endl;
    }
};

class Mechanic : public Job {
    public:
    void work(){
        cout << "Mechanic repairs the vehicles" << endl;
    }
};

class Teacher : public Job{
    public:
    void work(){
        cout << "Teacher teaches to students" << endl;
    }
};

int main(){
    Mechanic Mechanichal;
    Teacher Sir;
    Mechanichal.work();
    Mechanichal.salary();
    Sir.work();
}

