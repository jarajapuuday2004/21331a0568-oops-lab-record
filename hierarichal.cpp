#include<iostream>
using namespace std;
class Marine{
    public:
    void eat(){
        cout<<"I can eat!"<<endl;
    }
};
class Fish : public Marine{
    public:
    void breathe(){
        cout<<"I can breathe through gills!"<<endl;
    }
};
class Shark : public Marine {
    public:
    void hunt(){
        cout<<"I can hunt other animals"<<endl;
    }
};
int main(){
    Fish f;
    f.eat();
    f.breathe();

    Shark s;
    s.eat();
    s.hunt();
}