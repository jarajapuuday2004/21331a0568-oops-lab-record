#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
    int priVar;
    protected :
    int proVar;
    public:
    int pubVar;
    void setVar(int priValue, int proValue, int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar(){
    cout << "Private : "<< priVar << "Protected : " << proVar << "Public : " << pubVar << endl;
    }
};
int main(){
    AccessSpecifierDemo ob1;
    ob1.setVar(100, 220, 2000);
    ob1.getVar();
}