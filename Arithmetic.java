import java.util.*;
public class Arithmetic {
   public static void main(String[] args) {
       int x=0;
       Scanner input=new Scanner(System.in);
       System.out.println("Enter two numbers: ");
       int p = input.nextInt();
       int q = input.nextInt();
       System.out.println("Enter the arithmetic operator: ");
       char ch=input.next().charAt(0);
       if(ch=='+')
       {
           System.out.println("Sum is " + (p+q));
       }
       else if(ch=='-')
       {
        System.out.println("Difference is " + (p-q));
       }
       else if(ch=='*')
       {
        System.out.println("Multiplication is " + (p*q));
       }
       else if(ch=='/')
       {
        System.out.println("Division is " + (p/q));
       }
       else if(ch=='%')
       {
        System.out.println("Modulo Division is " + (p%q));
       }
       else
       {
        System.out.println("Not exist");
       }
   }
}

