import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JRadioButton;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;
public class Reg {
    Reg(){
        Frame fr = new Frame();
        fr.setTitle("Registration Form");
        fr.setSize(500, 500);
        fr.setVisible(true);
        fr.setBackground(Color.ORANGE);
        fr.setLayout(null);

        Label l1 = new Label("Name:");
        l1.setBounds(70, 50, 50, 50);
        TextField t1 = new TextField();
        t1.setBounds(150, 55, 230, 30);

        Label l2 = new Label("Roll No:");
        l2.setBounds(70, 100, 50, 50);
        TextField t2 = new TextField();
        t2.setBounds(150, 105, 230, 30);

        Label l3 = new Label("Mobile:");
        l3.setBounds(70, 150, 50, 50);
        TextField t3 = new TextField(10);
        t3.setBounds(150, 150, 230, 30);

        Label l4 = new Label("Gender:");
        l4.setBounds(70, 200, 50, 50);
        Checkbox r1,r2;
        CheckboxGroup cbg = new CheckboxGroup();
        r1 = new Checkbox("Male", cbg, false);
        r1.setBounds(150, 200, 65, 40);
        r2 = new Checkbox("Female", cbg, false);
        r2.setBounds(220, 200, 100, 40);
        
        Label l5 = new Label("Email:");
        l5.setBounds(70, 250, 50, 50);
        TextField t4 = new TextField();
        t4.setBounds(150, 255, 230, 30);

        Label l6 = new Label("Event:");
        l6.setBounds(70, 300, 50, 50);
        Choice c = new Choice();
        c.setBounds(150, 310, 100, 100);
        c.add("Codathon");
        c.add("Cadathon");
        c.add("Ideathon");
        c.add("Techtalk");
        c.add("Debug");

        Label l7= new Label();
        l7.setBounds(120, 400, 200, 50);
        Font f = new Font("times new roman", Font.BOLD, 15);
        l7.setFont(f);
        Button b = new Button("Submit");
        b.setBounds(160, 365, 70, 30);
        b.setBackground(Color.GREEN);
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                l7.setText("Registration Successful");
                System.out.println("Registration Successful\n");
                System.out.println("Details of Participant:");
                System.out.println("Name:"+ t1.getText());
                System.out.println("Roll No : " + t2.getText());
                System.out.println("Mobile: " + t3.getText());
                System.out.println("Email: " + t4.getText());
                System.out.println("Event Selected : " + c.getSelectedItem());
            }
        });
        fr.add(l1);
        fr.add(t1);
        fr.add(l2);
        fr.add(t2);
        fr.add(l3);
        fr.add(t3);
        fr.add(l4);
        fr.add(r1);
        fr.add(r2);
        fr.add(l5);
        fr.add(t4);
        fr.add(b);
        fr.add(l6);
        fr.add(c);
        fr.add(l7);

        fr.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                fr.dispose();
            }
        });
    } 
    public static void main(String[] args) {
        Reg r = new Reg();
    }  
}







