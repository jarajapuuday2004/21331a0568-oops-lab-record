import java.util.*;
class MapInter {
    public static void main(String[] args) {
    // Creating a map to store countries and their codes 	
    Map<String, Integer> codes = new HashMap<>();

    boolean emp = codes.isEmpty();
    System.out.println("Is Map empty? " +emp); 

    // Adding entries in the map.
    codes.put("Australia",61);
    codes.put("Bangladesh",880);
    codes.put("Ukraine",380);
    codes.put("China",86);
    codes.put("India",91);
    codes.put("USA",2);
    System.out.println("Country Codes : "+ codes);  

    //size of map
    int size = codes.size();
    System.out.println("No. of entries in Map: " +size);

    // Creating another map.
    Map<String,Integer> codes1 = new HashMap<>();
    codes1.put("Russia",7);
    codes1.put("Singapore",65);
    codes1.put("SriLanka",94);
    System.out.println("Codes1 map: " + codes1);

    // Inserting all elements of codes1 into codes
    codes.putAll(codes1);
    System.out.println("Updated Codes List: " + codes);

    // Remove
    codes.remove("SriLanka");
    System.out.println("After removing sriLanka code : " + codes);
    codes.remove("Singapore",65);
    System.out.println("After removing Singapore: " + codes);

    // Replacing 
    codes.replace("USA", 2, 1);
    System.out.println("Corrected codes : " + codes);

    //accessing value
    int c = codes.get("India") ; 
    System.out.println("Code of India : " + c);
    
    //check for presence of keys and values
    boolean Key = codes.containsKey("Thailand");
    System.out.println("Codes contains Thailand? " +Key);
    boolean Val = codes.containsValue(91);
    System.out.println("Code contains value 91? " + Val);

    //printing all the values in map
    System.out.println("Country codes available : " +codes.values());

    // clearing
    codes1.clear();
    System.out.println("Codes1 after clearing : "+ codes1);
    }
}

