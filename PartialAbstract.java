// Abstract class
abstract class Vehicle {
    // Abstract method
    public abstract void start();
    
   
    public void stop() {
        System.out.println("Vehicle stopped.");
    }
}




class Scooter extends Vehicle {
    public void start() {
        System.out.println("Scooter started.");
    }
}




class MotorBike extends Vehicle {
    public void start() {
        System.out.println("MotorBike started.");
    }
}




public class PartialAbstract {
    public static void main(String[] args) {
        Scooter obj1 = new Scooter();
        MotorBike obj2 = new MotorBike();
        
        obj1.start(); 
        obj2.start(); 
        
        obj1.stop(); 
        obj2.stop(); 
    }
}


