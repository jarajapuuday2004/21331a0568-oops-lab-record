#include<iostream>
using namespace std;
class Animal{
    public:
    void sleep(){
        cout<<"Animal is sleeping"<<endl;
    }
    protected:
    void eat(){
        cout<<"Animal is eating"<<endl;
    }
    private:
    void swim(){
        cout<<"Animal is swimming"<<endl;
    }
};
class FishPrivate : private Animal{
    public:
    void doSomething(){
        sleep();
        eat();
        //swim();
    }
};
class WhaleProtected : protected Animal{
    public:
    void doSomething(){
        sleep();
        eat();
        //swim();
    }
};
class DolphinPublic : public Animal{
    public:
    void doSomething(){
        sleep();
        eat();
        //swim();
    }
};
int main(){
    FishPrivate fish;
    WhaleProtected whale;
    DolphinPublic dolphin;
    fish.doSomething();
    whale.doSomething();
    dolphin.doSomething();
    return 0;
}

