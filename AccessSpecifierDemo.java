class AccessSpecifierDemo{
    private
        int priVar;
    protected
        int proVar;
    private
        int pubVar;




    public
         void setVar(int priValue,int proValue, int pubValue){
            priVar = priValue;
            proVar = proValue;
            pubVar = pubValue;
         }


    public
        void getVar(){
            System.out.println("Private variable is : " +priVar);
            System.out.println("Protected variable is : " +proVar);
            System.out.println("Public variable is : " +pubVar);
        }


    public static void main(String[] args){
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(10,79,89);
        obj.getVar();
    }
}