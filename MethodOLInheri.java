class Parent{
    void add(int a){
    System.out.println("Method with one param a+2="+(a+2));
    }
    void add(int a,int b){
    System.out.println("Method with two param a+b="+(a+b));
    }
    }
    class Child extends Mains{
    void add(int a,int b,int c){
    System.out.println("Method with three param a+b+c="+(a+b+c));
    }
    }
    public class MethodOLInheri {
    public static void main(String[] args) {
    Child obj=new Child();
    obj.add(69);
    obj.add(68,69);
    obj.add(67,68,69);
    }
    }