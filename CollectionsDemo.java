import java.io.StringWriter;
import java.util.LinkedHashSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;

public class CollectionsDemo {
    public static void main(String[] args) {
       
        //Vector
        Vector<String> sweets = new Vector<String>(7);
        System.out.println("Vector");
        System.out.println("Is Sweets empty? " + sweets.isEmpty());
        sweets.add("Jilebi");
        sweets.add("Rasgulla");
        sweets.add("Gulab Jamun");
        sweets.add("Halwa");
        sweets.add("Rasmalai");
        System.out.println("Sweets: " +sweets);

        //inserting at specific index
        sweets.insertElementAt("Kesari", 4);
        System.out.println("Sweets after insertion : "+ sweets);

        //capacity of vector
        System.out.println("Capacity of sweets:" + sweets.capacity());
        //first and last elements
        System.out.println("First element: " + sweets.firstElement());
        System.out.println("Last element: " + sweets.lastElement());

        //element at given index
        System.out.println("Element at index 2: "+ sweets.elementAt(2));

        //index of given element
        System.out.println("Index of Halwa:" + sweets.indexOf("Halwa"));

        //removing elements
        sweets.remove(4);
        sweets.remove("Rasgulla");
        System.out.println("After Deletion : " + sweets);
        //sweets.remove(10);

        //size of vector
        System.out.println("Size of sweets: " + sweets.size());
        sweets.setSize(10);
        System.out.println("New Size of sweets: " + sweets.size());

        //Removing all elements
        sweets.removeAllElements();
        System.out.println("After removing all elements: " + sweets);


         //Priority Queue
        Queue<String> home = new PriorityQueue<String>();
        System.out.println("\nPriority Queue");
        home.isEmpty();
        System.out.println("Home empty? "+ home.isEmpty());

        //adding
        home.add("Fan");
        home.add("Lights");
        home.add("Fridge");
        System.out.println("Home: " + home);
        home.offer("Oven");
        home.offer("Tv");
        System.out.println("Home after offer: " + home);

        //removing
        System.out.println("Removed element:" + home.remove());
        System.out.println("Home:"+ home);
        System.out.println("Polled element:" + home.poll());
        System.out.println("Home: " +home );

        //element and peek
        System.out.println("Element: "+ home.element());
        System.out.println("Peek element: "+ home.peek());

        //contains
        System.out.println("Is home have Mobile? "+ home.contains("Mobile"));

        //clear
        home.clear();
        System.out.println("Home: "+ home);



        //Linked Hash Set
        LinkedHashSet<String> fruits = new LinkedHashSet<>();
        System.out.println("\nLinked HashSet");

        //empty
        System.out.println("Is fruits Empty? " + fruits.isEmpty());

        //add
        fruits.add("Mango");
        fruits.add("Guava");
        fruits.add("Apple");
        fruits.add("Pineapple");
        System.out.println("Fruits:" + fruits);

        //Other linkedhashset
        LinkedHashSet<String> fruits1 = new LinkedHashSet<>();
        fruits1.add("Papaya");
        fruits.add("Pineapple");
        fruits.add("Grapes");
        //addall
        fruits.addAll(fruits1);
        System.out.println("Updated Fruits: " + fruits);

        //remove
        fruits.remove("Pineapple");
        System.out.println("After removing:" + fruits);
        fruits.remove("WaterMelon");

        //hashcode
        System.out.println("Hashcode of Fruits: " + fruits.hashCode());

        //contains
        System.out.println("Fruits contains Ginger?" + fruits.contains("Ginger"));

        //size
        System.out.println("Size: " + fruits.size());

        //clear
        fruits.clear();
        System.out.println("Fruits:" + fruits);

    }
}

