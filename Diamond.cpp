#include<iostream>
using namespace std;
class Marine {
    public :
    void sleep(){
        cout<<"I am sleeping"<<endl;
    }
};
class Fish : virtual public Marine{
    public :
    Fish(){
        cout<<"Fish is sleeping"<<endl;
    }
};
class GoldFish : virtual public Marine{
    public:
    GoldFish(){
        cout<<"GoldFish is sleeping"<<endl;
    }
};
int main()
{
    GoldFish gd;
    gd.sleep();
}