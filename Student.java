import java.lang.*;

class Student {

    String fullName;

    int rollNum;

    double semPercentage;

    String collegeName;

    int collegeCode;


    //constructor

    public Student(String fname,int roll,double percentage,String college,int code){

        fullName=fname;

        rollNum=roll;

        semPercentage=percentage;

        collegeName=college;

        collegeCode=code;

    }


    //destructor

    protected void finalize(){

        System.out.println("Destroyed");

    }


    //display method

    public void display(){

        System.out.println("Student details:");

        System.out.println("Full name :" + fullName);

        System.out.println("Roll No. :"+ rollNum);

        System.out.println("Sem Percentage :"+semPercentage);

        System.out.println("College Name :"+collegeName);

        System.out.println("College Code :"+collegeCode);

    }


    public static void main(String[] args) {

        Student k = new Student("Krishna", 584,98, "MVGR", 33);

        Student r = new Student("Radha", 385,99, "MVGR", 33);

        k.display();

        r.display();

    }

}


