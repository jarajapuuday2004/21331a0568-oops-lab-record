class Brand {
    String brandName;
    Brand(String name) {
        brandName = name;
    }
}
class Pen extends Brand {
    String modelName;
    Pen(String brand, String model) {
        super(brand); // Call the constructor of the parent class with the brand name
        modelName = model;
    }
    void showPen() {
        System.out.println("Brand Name: " + brandName);
        System.out.println("Model Name: " + modelName);
    }
}

public class Simple {
    public static void main(String[] args) {
        Pen m = new Pen("REYNOLDS", "045 BALL PENS");
        m.showPen();
    }
}