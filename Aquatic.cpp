#include<iostream>
using namespace std;
class Aquatic{
    public:
    void swim(){
        cout<<"Swimming"<<endl;
    }
};
class Animal{
    public:
    void eat(){
        cout<<"Eating"<<endl;
    }
};
class Fish : public Aquatic,public Animal{
    public:
    void breathe(){
        cout<<"Breathing"<<endl;
    }
};
int main(){
    Fish f;
    f.swim();
    f.eat();
    f.breathe();
}