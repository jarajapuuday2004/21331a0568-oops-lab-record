#include<iostream>
using namespace std;
class Marine{
    public:
    void swim(){
        cout<<"I can swim!"<<endl;
    }
    void eat(){
        cout<<"I can eat!"<<endl;
    }
};
class Fish : public Marine{
    public:
    void breathe(){
        cout<<"I can breathe through gills!"<<endl;
    }
};
int main(){
    Fish fish1;
    fish1.swim();
    fish1.eat();
    fish1.breathe();
}