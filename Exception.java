import java.util.*;
class Exception{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter two numbers : ");
            float a = input.nextFloat();
            float b = input.nextFloat();
            System.out.println("The value of a/b is : " +a/b);
        }
        catch(ArithmeticException obj){
            System.out.println("Divide by zero error!!!");
        }catch(InputMismatchException obj){
            System.out.println("Only integers are accepted!!!");
        }
    }
}