#include<iostream>

using namespace std;

class Student {

    string fullName;

    int rollNum;

    double semPercentage;

    string collegeName;

    int collegeCode;


    public:

    //constructor

    Student(string fname,int roll,double percentage,string college,int code) {

        fullName = fname;

        rollNum = roll;

        semPercentage = percentage;

        collegeName = college;

        collegeCode = code;

    }

    //Display method

   void displayData()

    {

        cout<<"Student Details"<<endl;

        cout<<"Full Name : "<<fullName<<endl;

        cout<<"Roll No. : "<<rollNum<<endl;

        cout<<"Sem Percentage : "<<semPercentage<<endl;

        cout<<"College Name : "<<collegeName<<endl;

        cout<<"College Code : "<<collegeCode<<endl;

        cout<<" "<<endl;

    }

    //Destructor

    ~Student() {

        cout<<"Object Destructed"<<endl;

    }

};

int main()

{

    Student r("Uday",568,88,"MVGR",33);

    Student s("Sita",385,90,"MVGR",33);

    r.displayData();

    s.displayData();

    return 0;

}

