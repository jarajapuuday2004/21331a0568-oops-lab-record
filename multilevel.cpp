#include<iostream>
using namespace std;
class Marine{
    public:
    void sleep(){
        cout<<"I am sleeping"<<endl;
    }
};
class Fish : public Marine{
    public:
    void eat(){
        cout<<"I am eating!"<<endl;
    }
};
class GoldFish : public Fish{
    public:
    void beautiful(){
        cout<<"I am looking beautiful"<<endl;
    }
};
int main(){
    GoldFish g;
    g.sleep();
    g.eat();
    g.beautiful();
}