#include<iostream>
using namespace std;
class Video{
    public:
        virtual void Capture()=0;
        
};


class NikonCamera : public Video {
    public :
    void Capture(){
        cout<< " Nikon Camera captures a Video "<<endl;
    }
};


class SamsungCamera : public Video {
    public:
    void Capture(){
        cout<<" Samsung Camera captures a Video "<<endl;
    }
};


int main(){
    NikonCamera obj1;
    obj1.Capture();
    SamsungCamera obj2;
    obj2.Capture();
}