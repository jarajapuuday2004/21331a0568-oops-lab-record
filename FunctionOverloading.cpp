#include<iostream>
using namespace std;
class FuncOverLoad{
    public:
    void add(int a, double b)
    {
	cout<<"sum = "<<(a+b);
    }

    void add(double a, int b)
    {
	    cout<<endl<<"sum = "<<(a+b);
    }
};


// Driver code
int main()
{
    FuncOverLoad obj;
	obj.add(10,2.5);
	obj.add(5.5,6);

	return 0;
}
