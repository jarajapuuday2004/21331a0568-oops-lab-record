import java.util.*;
class Threads extends Thread{
    public void run(){
        try{
            System.out.println("Enter a number : ");
            Scanner input = new Scanner(System.in);
            int num = input.nextInt();
            multiply(num);
        }catch(InputMismatchException obj){
            System.out.println("Only Integers are allowed");
        }
    }
public void multiply(int num){
    for(int i=0;i<=12;i++){
        System.out.println(num+"*"+i+"="+num*i);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
public static void main(String[] args){
    Threads t1 = new Threads();
    Threads t2 = new Threads();
    t1.start();
    t2.start();
}
}