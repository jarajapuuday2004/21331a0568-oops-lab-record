class Admin{
    String a ="Secret";
    void display() {
        System.out.println("Admin Method");
    }
}
class User1 extends Admin {
    void display() {
        System.out.println("User1 Method");
    }
}
class User2 extends Admin {
    void display() {
        System.out.println("User2 Method");
    }
} 
class User extends User1,User2 {  // Error 
    public static void main(String[] args) {
        User u = new User();
        u.display();
    }
}