class Parent {
	void affection()
	{
		System.out.println("Parent's affection");
	}
};
 class Child extends Parent {
	@Override
	void affection()
	{
		System.out.println("Child's affection");
	}
};

// Driver class
class Childs {
	public static void main(String[] args)
	{
		Parent obj1 = new Parent();
		obj1.affection();

		Parent obj2 = new Child();
		obj2.affection();
	}
}